Python script to get movies playing in theatres from Rotten Tomatoes API and actor info from Wikipedia.

* **Postgres**:  
   user is *test* identified by *test*  
   assumes location is localhost  

* Prerequisites for **movies.py**:  
    - BeautifulSoup  
    - requests  
    - lxml  
    - psycopg2  
  
* **movies.sql** contains ddl:  
   Two tables 'movies' and 'actors'  
   movies table holds foreign key to actors  
   One to Many association  

* TODO:  
   Create reference table for movie-actor relations in order to support many to many association and more than 5 actors per movie.  

* **omdb.py** is independent. Get movie info from IMBD based on user input.  