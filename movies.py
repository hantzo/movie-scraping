from bs4 import BeautifulSoup
from urllib2 import urlopen
import requests
import json
import lxml
import urllib
import psycopg2
import re



def getMovies():

    # Get list o movies now in theaters using rotten tomatoes' API and call getWiki function for each actor in every movie
    key = "qtqep7qydngcc7grk4r4hyd9"
    url = "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?apikey="+key
    res = requests.get(url)
 
    data = res.content
 
    js = json.loads(data)
    movies = js["movies"]

    for movie in movies:
        for actor in movie['abridged_cast']:
            actor['wikinfo'] = getWiki(actor['name'])

        writeMov(movie)
   	for actor in movie['abridged_cast']:
   	    writeActor(actor)



def getWiki(actor):

    # Create Wiki URL for actor and call getParagraph to fetch data from Wikipedia
    base_url = 'https://www.wikipedia.org/wiki/'
    url = base_url + actor.replace(' ','_')

    try:
        info = getParagraph(url)
    except:
        info = 'No Wiki entry for %s.'%actor

    return info


def getParagraph(addr):

    # Get and parse html file from Wikipedia from addr. Return first paragraph as string.
    soup = BeautifulSoup(urlopen(addr),"lxml")
    text = soup.body.find('p')
    target = ''
    
    for i in text:
        target += str(i)

    parag =  re.sub('<.*?>', '', target)
    
    return parag


def writeMov(mov):


    conn = psycopg2.connect("dbname='movies' user='test' host='localhost' password='test'")

    try:
        cur = conn.cursor()

        # Commit to movies table
        try:
            cur.execute("INSERT INTO movies (title,release,actor1,actor2,actor3,actor4,actor5) VALUES (%s, %s, %s, %s, %s, %s, %s)",
			 (mov['title'], mov['year'], mov['abridged_cast'][0]['id'], mov['abridged_cast'][1]['id'],
                          mov['abridged_cast'][2]['id'], mov['abridged_cast'][3]['id'] ,mov['abridged_cast'][4]['id']))
        except psycopg2.IntegrityError:
            conn.rollback()
        else:
            conn.commit()
        cur.close()
    except Exception , e:
        print 'ERROR:', e[0]


def writeActor(act):

    conn = psycopg2.connect("dbname='movies' user='test' host='localhost' password='test'")

    try:
        cur = conn.cursor()
        # Commit to actors table
        try:
            cur.execute("INSERT INTO actors (name, id, wikinfo) VALUES (%s, %s, %s)", (act['name'], act['id'], act['wikinfo']))
        except psycopg2.IntegrityError:
            conn.rollback()
	    #cur.execute("UPDATE actors SET wikinfo=(%s) WHERE id = (%s)", (act['wikinfo'],act['id'])
	else:
            conn.commit()
        cur.close()
    except Exception , e:
        print 'ERROR:', e[0]



if __name__ == "__main__":
    getMovies()


